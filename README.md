# malli-select

This Clojure library allows for creating subschemas of existing [malli](https://github.com/metosin/malli)-schemas.

It's based on Rich Hickey's ideas from his talk ["Maybe Not"](https://youtu.be/YR5WdGrpoug?feature=shared&t=1965) about how [spec-alpha2](https://github.com/clojure/spec-alpha2) might allow for schema reuse.

## Usage

Add to `deps.edn`:
``` clojure
:deps { ,,,
       ;; bring Malli
       ;; metosin/malli {:mvn/version "0.12.0"}
       com.github.eval/malli-select {:git/sha ",,,"}
}
```

## Examples

Given the following schema:

``` clojure
(def Person
    [:map
     [:name string?]
     [:age [:int {:min 0 :max 99}]]
     [:address [:map
                [:street string?]
                [:number int?]
                [:country [:map
                           [:iso string?]
                           [:name string?]]]]]])
```

Some example selections:

``` clojure
(require '[dk.thinkcreate.malli-select :refer [select]])

;; everything optional
(select Person [])
;; => [:map
       [:name {:optional true} string?]
       [:age {:optional true} [:int {:min 0, :max 99}]]
       [:address
        {:optional true}
        [:map
         [:street {:optional true} string?]
         [:number {:optional true} int?]
         [:country
          {:optional true}
          [:map
           [:iso {:optional true} string?]
           [:name {:optional true} string?]]]]]]


;; Option `:prune-optionals` is used in the following examples
;; to make the resulting schemas less verbose.

;; require `:name` and `:age`
(select Person [:name :age] {:prune-optionals true)
;; => [:map [:name string?] [:age [:int {:min 0, :max 99}]]]

;; require :address (and everything underneath it)
(select Person [:address] {:prune-optionals true})
;; => [:map
       [:address
        [:map
         [:street string?]
         [:number int?]
         [:country [:map [:iso string?] [:name string?]]]]]]

;; *IF* address is provided, then it should have a `:street`
(select Person [{:address [:street]}] {:prune-optionals true})
;; => [:map
       [:address
        {:optional true}
        [:map
         [:street string?]]]]

;; require everything but from :address only :street
(select Person ['* {:address [:street]}] {:prune-optionals true})
;; => [:map
       [:name string?]
       [:age [:int {:min 0, :max 99}]]
       [:address [:map [:street string?]]]]

;; Selecting something not in the schema yields an assertion error
(select Person [:foo])
   Assert failed: Selection contains unknown paths: ([:foo])

   Available: ([] [:address] [:age] [:name] [:address :country]
   [:address :number] [:address :street] [:address :country :iso]
   [:address :country :name])

   (empty? unknown-selections)

;; A `:maybe` or `:vector` may wrap the `:map`
(select [:maybe Person] [:name] {:prune-optionals true}))
;; => [:maybe [:map [:name string?]]]

;; same goes for :map-of, :vector
(-> [:map-of string? [:map [:name string?] [:iso string?]]]
  (select [:name] {:prune-optionals true}))
  (m/validate {"NL" {:name "The Netherlands"}})) ;; => true

```

## Disclaimer

Beware that this library was never tested in production: It might contain bugs and, although I did some benchmarking, it might be slow.
After having written various variations of 'select' (all with varying degrees of success) in some of my pet projects, I thought it might be nice to work out the ideas of a select for malli and release it in the public domain.


## LICENSE

Copyright (c) 2023 Gert Goet, ThinkCreate
Distributed under the MIT license. See LICENSE.
